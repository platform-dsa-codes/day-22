import java.util.HashMap;
import java.util.Map;

class Solution {
    public int subarraySum(int[] nums, int k) {
        // Map to store cumulative sum and its frequency
        Map<Integer, Integer> sumFrequency = new HashMap<>();
        sumFrequency.put(0, 1); // Initialize with 0 as a sum encountered once.

        int sum = 0; // Cumulative sum
        int count = 0; // Counter for subarrays with sum k

        for (int num : nums) {
            sum += num;
            // Check if there is a subarray ending at the current index with sum k
            if (sumFrequency.containsKey(sum - k)) {
                count += sumFrequency.get(sum - k); // Update count
            }
            // Update sumFrequency map
            sumFrequency.put(sum, sumFrequency.getOrDefault(sum, 0) + 1);
        }

        return count;
    }
}
