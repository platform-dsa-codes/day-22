//{ Driver Code Starts
import java.util.*;
import java.io.*;
import java.lang.*;

class Main
{
    public static void main(String args[])
    {
        //taking input using class Scanner
        Scanner sc = new Scanner(System.in);
        
        //taking total count of testcases
        int t = sc.nextInt();
        
        
        
        while(t-- > 0)
        {
            //taking total number of elements
            int n = sc.nextInt();
            
            //taking size of subArrays 
            int k = sc.nextInt();
            
            //Declaring and Intializing an array of size n
            int arr[] = new int[n];
            
            //adding all the elements to the array 
            for(int i = 0; i <n; i++)
            {
                arr[i] =sc.nextInt();
            }
            
            //Calling the method max_of_subarrays of class solve
            //and storing the result in an ArrayList
            ArrayList <Integer> res = new Solution().max_of_subarrays(arr, n, k);
            
            //printing the elements of the ArrayList
            for (int i = 0; i < res.size(); i++)
                System.out.print (res.get (i) + " ");
            System.out.println();
        }
    }
}
// } Driver Code Ends

class Solution
{
    // Function to find maximum of each subarray of size k.
    static ArrayList<Integer> max_of_subarrays(int arr[], int n, int k)
    {
        ArrayList<Integer> result = new ArrayList<>();
        Deque<Integer> deque = new LinkedList<>();

        // Process the first k elements separately
        int i;
        for (i = 0; i < k; i++) {
            // Remove elements from the deque that are smaller than the current element
            while (!deque.isEmpty() && arr[i] >= arr[deque.peekLast()])
                deque.removeLast();

            // Add the current index to the deque
            deque.addLast(i);
        }

        // Process the remaining elements
        for (; i < n; i++) {
            // The front of the deque contains the maximum element for the previous window
            result.add(arr[deque.peek()]);

            // Remove elements from the deque that are out of the current window
            while (!deque.isEmpty() && deque.peek() <= i - k)
                deque.removeFirst();

            // Remove elements from the deque that are smaller than the current element
            while (!deque.isEmpty() && arr[i] >= arr[deque.peekLast()])
                deque.removeLast();

            // Add the current index to the deque
            deque.addLast(i);
        }

        // Add the maximum element of the last window
        result.add(arr[deque.peek()]);

        return result;
    }
}
